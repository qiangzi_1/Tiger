/**
 * 记录数据Service
 */
package cn.zhucongqi.tiger.services;

import java.math.BigInteger;
import java.util.List;

import cn.zhucongqi.tiger.db.models.DeviceToken;
import cn.zhucongqi.tiger.db.models.TigerRecord;
import cn.zhucongqi.tiger.kit.DDLMakerKit;
import cn.zhucongqi.tiger.kit.UserAgentKit;

import com.jfinal.ext2.core.Service;
import com.jfinal.ext2.kit.DateTimeKit;
import com.jfinal.ext2.kit.JsonExtKit;
import com.jfinal.plugin.activerecord.Db;

/**
 * 数据记录 Service
 * @author BruceZCQ [zcq@zhucongqi.cn]
 * @version
 */
public class CodeDataService extends Service {

	/**
	 *  插入数据
	 * @param code
	 * @param clList
	 */
	public void logData(String token, String code, String jsondata){
		
		if (!isTimeOut(token)) {
			this.controller.renderNull();
		}
		
		String platformName = UserAgentKit.platformName(this.controller.getRequest().getHeader("User-Agent"));
		// update info id
		String infoTableName = DDLMakerKit.codeInfoTableName(code, platformName);
		TigerRecord info = new TigerRecord();
		Db.save(infoTableName, info);
		// save data to data table ref hashid
		String dataTableName = DDLMakerKit.codeDataTableName(info.getBigInteger("id"), code, platformName);
		TigerRecord data = (TigerRecord)JsonExtKit.jsonToRecord(jsondata);
		data.update();
		Db.save(dataTableName, data);
	}
	
	/**
	 * token 是否过期
	 * @param tocken
	 * @return
	 * TODO 3小时内才算过期
	 */
	public boolean isTimeOut(String token){
	
		List<DeviceToken> deviceTokens = DeviceToken.dao.find("SELECT * FROM "
				+ DeviceToken.table + " WHERE token = ?  LIMIT 1 ", token);
		if (null != deviceTokens && deviceTokens.size() == 1) {
			BigInteger timeout = deviceTokens.get(0).get("timeout");
			return DateTimeKit.getCurrentUnixTime() < Long.valueOf(timeout.toString()) ;
		}
		return false;
	}
	
}
