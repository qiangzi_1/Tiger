/**
 * 
 */
package cn.zhucongqi.tiger.controllers;

import cn.zhucongqi.tiger.consts.ViewPaths;

import com.jfinal.ext2.core.ControllerExt;
import com.jfinal.ext2.kit.PageViewKit;

/**
 * 
 * @author BruceZCQ [zcq@zhucongqi.cn]
 * @version
 */
public class DashboardController extends ControllerExt {

	
	public void index(){
		this.indexV();
	}
	
	private void indexV(){
		this.render(PageViewKit.getJSPPageViewFromWebInf(ViewPaths.DASHBOARD_VIEW_PATH, "index"));
	}
	
}
